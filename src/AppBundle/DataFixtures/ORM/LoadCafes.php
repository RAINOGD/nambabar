<?php
/**
 * Created by PhpStorm.
 * User: rain
 * Date: 8/2/17
 * Time: 5:57 PM
 */

namespace AppBundle\DataFixtures\ORM;
use AppBundle\Entity\Cafe;
use AppBundle\Entity\Dish;
use Doctrine\Common\DataFixtures\FixtureInterface;

use Doctrine\Common\Persistence\ObjectManager;

class LoadCafes implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $photos = ['first.jpg', 'second.jpeg'];
        for ($i = 0; $i <= 1; $i++){
            $cafe = new Cafe();
            $cafe->setName('Cafe ' . $i)
                ->setDescription('We are Cafe - ' . $i)
                ->setImageName($photos[$i]);

            $manager->persist($cafe);
            $manager->flush();
            for ($j = 1; $j <= 2; $j++){
                $dish = new Dish();

                $dish->setName('Dish - ' . $j . $i)
                    ->setDescription('Tasty dish - ' . $i . $j)
                    ->setPrice(($i + 1) * $j * 3 + rand(1, 30))
                    ->setCafe($cafe);
                $manager->persist($dish);
                $manager->flush();
            }
        }
    }
}