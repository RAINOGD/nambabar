<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cafe;
use AppBundle\Entity\Dish;
use AppBundle\Entity\Purchases;
use AppBundle\Form\orderType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DishesController extends Controller
{
    /**
     * @Method({"GET", "POST"})
     * @Route("/cafe/{cafe_id}/main-page")
     * @param int $cafe_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(int $cafe_id)
    {
        $cafe = $this->getDoctrine()
            ->getRepository('AppBundle:Cafe')
            ->find($cafe_id);

        /** @var Dish[] $dishes */
        $dishes = $cafe->getDishes();
        $dishesForm = [];

        foreach ($dishes as $dish){
            $dishesForm[$dish->getId()] = $this->createForm(orderType::class, null, [
                'action' => $this->generateUrl('app_dishes_addtobucket', [
                    'dish_id' => $dish->getId()
                ]),
                'method' => 'POST'
            ])->createView();
        }
        $totalPrice = 0;
        $bucketOfThisCafe = null;
        $session = $this->get('session');
        if ($session->has('bucket')){
            $bucket = $session->get('bucket');
            if (isset($bucket[$cafe_id])){
                $bucketOfThisCafe = $bucket[$cafe_id];
                foreach ($bucketOfThisCafe as $item){
                    $totalPrice += $item['price'];
                }
            }
        }

        return $this->render('AppBundle:Dishes:index.html.twig', array(
            'cafe' => $cafe,
            'dishesForm' => $dishesForm,
            'dishes' => $dishes,
            'bucket' => $bucketOfThisCafe,
            'total_price' => $totalPrice
        ));
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/add/{dish_id}")
     * @param Request $request
     * @param $dish_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addToBucket($dish_id, Request $request)
    {
        $form = $this->createForm(orderType::class);

        $count = null;

        $form->handleRequest($request);
        if($form->isSubmitted()) {
            $count = $form->getData()['count'];
        }

        $dish = $this->getDoctrine()
            ->getRepository('AppBundle:Dish')
            ->find($dish_id);

        $purchase = new Purchases();
        $purchase->setDish($dish)
            ->setQty($count)
            ->setDatetime(new \DateTime());
        $em = $this->getDoctrine()->getManager();
        $em->persist($purchase);
        $em->flush();

        $cafe_id = $dish->getCafe()->getId();

        $session = $this->get('session');
        $price = $count * $dish->getPrice();

        $dish_in_bucket = ['dish' => $dish->getName(), 'count' => $count, 'price' => $price];
        if (!$session->has('bucket')){
            $session->set('bucket', []);
        }

        $bucket = $session->get('bucket');
        $bucket[$cafe_id][] = $dish_in_bucket;
        $session->set('bucket', $bucket);

        return $this->redirectToRoute('app_dishes_index', [
            'cafe_id' => $dish->getCafe()->getId()
        ]);
    }

}
