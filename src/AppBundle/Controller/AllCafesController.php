<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AllCafesController extends Controller
{
    /**
     * @Method("GET")
     * @Route("/")
     */
    public function indexAction()
    {
        /** @var User $user */
            $cafes = $this->getDoctrine()->getRepository('AppBundle:Cafe')
                ->findAll();

            $date = (new \DateTime(date("Y")."-01-01"))
                ->format("Y-m-d");

            $popular_dishes = $this->getDoctrine()
                ->getRepository('AppBundle:Dish')
                ->getPopularDishes($date);

            return $this->render('AppBundle:AllCafes:index.html.twig', array(
                'cafes' => $cafes,
                'popular_dishes' => $popular_dishes
            ));
    }

    /**
     * @Method("GET")
     * @Route("/enter")
     */
    public function enterAction(){
        return $this->render('@App/Enter/enter.html.twig');
    }

}
