<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Purchases
 *
 * @ORM\Table(name="purchases")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PurchasesRepository")
 */
class Purchases
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime")
     */
    private $datetime;

    /**
<<<<<<< HEAD
     * @var \DateTime
=======
     * @var int
>>>>>>> editingBranch
     *
     * @ORM\Column(name="qty", type="integer")
     */
    private $qty;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Dish", inversedBy="purchases")
     * @ORM\JoinColumn(name="dish_id", referencedColumnName="id")
     */
    private $dish;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return Purchases
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set qty
     *
     * @param integer $qty
     *
     * @return Purchases
     */
    public function setQty($qty)
    {
        $this->qty = $qty;

        return $this;
    }

    /**
     * Get qty
     *
     * @return int
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * Set dish
     *
     * @param \AppBundle\Entity\Dish $dish
     *
     * @return Purchases
     */
    public function setDish(\AppBundle\Entity\Dish $dish = null)
    {
        $this->dish = $dish;

        return $this;
    }

    /**
     * Get dish
     *
     * @return \AppBundle\Entity\Dish
     */
    public function getDish()
    {
        return $this->dish;
    }
}
