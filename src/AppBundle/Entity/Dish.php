<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Dish
 *
 * @ORM\Table(name="dish")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DishRepository")
 */
class Dish
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal")
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cafe", inversedBy="dishes")
     * @ORM\JoinColumn(name="cafe_id", referencedColumnName="id")
     */
    private $cafe;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Purchases", mappedBy="dish")
     */
    private $purchases;


    public function __toString()
    {
        return $this->name ?: '';
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Dish
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Dish
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Dish
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set cafe
     *
     * @param \AppBundle\Entity\Cafe $cafe
     *
     * @return Dish
     */
    public function setCafe(\AppBundle\Entity\Cafe $cafe = null)
    {
        $this->cafe = $cafe;

        return $this;
    }

    /**
     * Get cafe
     *
     * @return \AppBundle\Entity\Cafe
     */
    public function getCafe()
    {
        return $this->cafe;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->purchases = new ArrayCollection();
    }

    /**
     * Add purchase
     *
     * @param \AppBundle\Entity\Purchases $purchase
     *
     * @return Dish
     */
    public function addPurchase(\AppBundle\Entity\Purchases $purchase)
    {
        $this->purchases[] = $purchase;

        return $this;
    }

    /**
     * Remove purchase
     *
     * @param \AppBundle\Entity\Purchases $purchase
     */
    public function removePurchase(\AppBundle\Entity\Purchases $purchase)
    {
        $this->purchases->removeElement($purchase);
    }

    /**
     * Get purchases
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPurchases()
    {
        return $this->purchases;
    }
}
